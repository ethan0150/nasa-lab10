package main

import (
	//"fmt"
	//"log"
	"main/handler"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/key", handler.CreateKVPair).Methods("POST")
	r.HandleFunc("/key", handler.GetAllValues).Methods("GET")
	r.HandleFunc("/key/{key:.*}", handler.GetValue).Methods("GET")
	r.HandleFunc("/key/{key:.*}", handler.UpdateValue).Methods("PUT")
	r.HandleFunc("/key/{key:.*}", handler.DeleteValue).Methods("DELETE")
	loggedRouter := handlers.LoggingHandler(os.Stdout, r)
	http.ListenAndServe(":8080", loggedRouter)
}

package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

var db = map[string]string{}

func CreateKVPair(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var decoded map[string]string
	err := decoder.Decode(&decoded)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(err)
		panic(err)
	}
	key := decoded["key"]
	val := decoded["value"]
	_, exist := db[key]
	if exist {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		db[key] = val
		w.WriteHeader(http.StatusCreated)
	}
}

func GetValue(w http.ResponseWriter, r *http.Request) {
	key := mux.Vars(r)["key"]
	val, exist := db[key]
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	if exist {
		err := json.NewEncoder(w).Encode(map[string]string{key: val})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			panic(err)
		}
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func GetAllValues(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	keys := []string{}
	for key := range db {
		keys = append(keys, key)
	}
	json.NewEncoder(w).Encode(keys)
}

func UpdateValue(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var decoded map[string]string
	err := decoder.Decode(&decoded)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(err)
		panic(err)
	}
	key := mux.Vars(r)["key"]
	val := decoded["value"]
	_, exist := db[key]
	db[key] = val
	if exist {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusCreated)
	}
}

func DeleteValue(w http.ResponseWriter, r *http.Request) {
	key := mux.Vars(r)["key"]
	_, exist := db[key]
	if exist {
		delete(db, key)
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}
